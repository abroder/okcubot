#!/usr/bin/env node

var fs = require('fs')
var Browser = require('zombie')

var questions = JSON.parse(fs.readFileSync(__dirname + '/questions.txt'))
var question = questions.splice(Math.floor(Math.random() * questions.length), 1)[0]

var browser = new Browser()
browser.visit('http://m.twitter.com/login', function () {
  if (!browser.success) {
    console.log('Problem accessing twitter. Stopping now.')
    return
  }

  var config = require('./config.js').twitter

  browser.fill('session[username_or_email]', config.username)
    .fill('session[password]', config.password)
    .pressButton('commit', function () {
      if (!browser.success) {
        console.log('Error logging in. Terminating.')
        return
      }

      browser.visit('http://twitter.com', function () {
        if (!browser.success) {
          console.log('Trouble redirecting to desktop site')
          return
        }

        var initData = JSON.parse(browser.query('#init-data').getAttribute('value'))
        var authenticityToken = initData.formAuthenticityToken

        var card_data = {
          'twitter:api:api:endpoint': '1',
          'twitter:card': 'poll' + question.choices.length + 'choice_text_only'
        }

        question.choices.forEach(function (choice, idx) {
          card_data['twitter:string:choice' + (idx + 1) + '_label'] = choice
        })

        var cardBody = 'authenticity_token=' + authenticityToken +
          '&card_data=' + encodeURIComponent(JSON.stringify(card_data))

        browser.fetch('https://twitter.com/i/cards/create/v1', { 
          method: 'POST', 
          body: cardBody,  
          headers: [['Content-Type', 'application/x-www-form-urlencoded']]
        }).
        then(function (response) {
          if (response.status === 200) {
            return response.json()
          }
        }).
        then(function (data) {
          var tweetBody = 'authenticity_token=' + authenticityToken +
           '&card_uri=' + encodeURIComponent(data.card_uri) +
           '&status=' + encodeURIComponent(question.question)

          return browser.fetch('https://twitter.com/i/tweet/create', {
            method: 'POST',
            body: tweetBody,
            headers: [['content-type', 'application/x-www-form-urlencoded; charset=UTF-8'],
                      ['referer', 'https://twitter.com']]
          })
        }).
        then(function (response) {
          if (response.status === 200) {
            fs.writeFileSync(__dirname + '/questions.txt', JSON.stringify(questions))
            fs.appendFileSync(__dirname + '/tweeted.txt', JSON.stringify(question) + '\n')
          } else {
            console.log('Problem posting poll.')
          }
          browser.tabs.closeAll()
        })
      })
    })
})

