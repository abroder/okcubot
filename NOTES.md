# Creating a Twitter Poll

Creating a poll on Twitter requires two POST requests - one to create a card
containing the poll, the other to post the tweet with the card.

## POST /i/cards/create/v1

`content-type: application/x-www-form-urlencoded; charset=UTF-8`

### Arguments

**`authenticity_token`** - the 
[CSRF token](https://en.wikipedia.org/wiki/Cross-site_request_forgery)
for the current session. Can be found in the JSON string stored in 
`#init-data[value]` with the key `formAuthenticityToken`.

**`card_data`** - A JSON string describing the contents of the poll. Example:

    {
      "twitter:api:api:endpoint":"1",
      "twitter:card":"poll4choice_text_only",
      "twitter:string:choice1_label":"Test",
      "twitter:string:choice2_label":"Test",
      "twitter:string:choice3_label":"Test",
      "twitter:string:choice4_label":"Test"
      "twitter:long:duration_minutes":4320
    }

Values of `twitter:card` can be anything from `poll2choice_text_only` to 
`poll4choice_text_only`. The number of choice labels should always start at
1 and there should be as many choices as described in `twitter:card`.

`twitter:long:duration_minutes` is optional and if omitted, the poll defaults
to 24 hours.

### Response

A successful response will a JSON string of the form

    {
      "card_uri":"card:\/\/729853284328968192","status":"OK"
    }

## POST /i/tweet/create

`content-type: application/x-www-form-urlencoded; charset=UTF-8`

### Arguments

**`authenticity_token`** - see above description

**`card_uri`** - the card\_uri received in response to the previous request

**`status`** - the text of the tweet

(Other optional arguments exist, such as `is_permalink_page`, `place_id`,
and `tagged_users`)

### Notes

The `referer` header must be set or you will receive a 403 response with the
message in the body "Your account may not be allowed to perform this action."

# TODO

- Rewrite the documentation for more clarity
- Find out more about the other optional arguments to `create` (likely has 
already been documented somewhere).
- Try breaking the current description - see what arguments are actually 
required to create the card, etc. Minimal set of header modification.
- What values work for `referer`?
- How long does it take for a card to expire?
- Min and max values of `twitter:long:duration_minutes`; maximum you can do
from web view is 7 days, 0 minutes, 0 hours (10080 minutes) and minimum is
5 minutes, but is that checked on the server?