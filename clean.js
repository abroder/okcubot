var fs = require('fs')

var questions = JSON.parse(fs.readFileSync(__dirname + '/all-questions.txt'))

questions = questions.map(function (question) {
  question.choices = question.choices.map(function (choice) {
    if (choice[choice.length - 1] === '.') {
      return choice.substr(0, choice.length - 1)
    } else {
      return choice
    }
  })
  return question
})

questions = questions.filter(function (question) {
  return question.question.length <= 116 &&
    question.choices.every(function (choice) { return choice.length < 25} )
})

fs.writeFileSync(__dirname + '/questions.txt', JSON.stringify(questions))