var fs = require('fs')
var Browser = require('zombie')

var questions = []

var updateFile = function () {
  var fileContents
  try {
    fileContents = JSON.parse(fs.readFileSync(__dirname + '/all-questions.txt'))
  } catch (ex) {
    fileContents = []
  }
  
  fileContents.concat(questions)
  fs.writeFileSync(__dirname + '/all-questions.txt', JSON.stringify(questions))
}

var scrapeQuestion = function (browser) {
  var questionElem = browser.querySelector('.question')

  // Get the question text.
  var questionTextElem = browser.query('.qtext', questionElem)
  var questionText = browser.text(questionTextElem)

  var answers = []
  var answerContainer = browser.query('.my_answer', questionElem)
  var answerElems = browser.queryAll('label.radio', answerContainer)
  for (var i = 0; i < answerElems.length; i++) {
    answers.push(browser.text(answerElems[i]))
  }

  var question = { question: questionText, choices: answers }
  questions.push(question)
  console.log(question)

  browser.clickLink('Skip question', function () {
    if (questions.length < 200 && browser.success) {
      try {
        scrapeQuestion(browser)  
      } catch (ex) {
        console.log('Error scraping question. Writing out questions that have been seen.')
        updateFile()
      }
    } else if (questions.length >= 200) {
      console.log('Finished scraping 200 questions. Saving...')
      updateFile()
      browser.destroy()
    } else {
      console.log('Error skipping question. Writing out questions that have been seen.')
      updateFile()
    }
  })
}

var browser = new Browser()
browser.visit('http://okcupid.com/', function () {
  if (!browser.success) {
    console.log('Problem accessing okcupid. Stopping now.')
    return
  }

  browser.fill('#login_username', '')
    .fill('#login_password', '')
    .pressButton('#sign_in_button', function () {
      browser.visit('http://okcupid.com/questions', function () {
        if (!browser.success) {
          console.log('Problem accessing questions. Stopping now.')
          return
        }

        scrapeQuestion(browser)
      })
    })
})
